using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    [SerializeField] float levelLoadDelay = 1f;
    [SerializeField] AudioClip crashSFX;
    [SerializeField] AudioClip successSFX;
    [SerializeField] ParticleSystem crashVFX;
    [SerializeField] ParticleSystem successVFX;

    AudioSource audioSource;

    bool isTransitioning = false;
    bool isCheatCollision = false;
   
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        CheatNextLevel();
        CheatCollision();
    }

    void CheatNextLevel()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadNextLevel();
        }
    }
    
    void CheatCollision()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            SwitchIsCheatCollision();
        }
    }

    void SwitchIsCheatCollision()
    {
        isCheatCollision = !isCheatCollision;
    }

    void OnCollisionEnter(Collision other)
    {
        if (isTransitioning || isCheatCollision) { return; }
        else { 
            switch (other.gameObject.tag)
            {
                case "Friendly":
                    Debug.Log("Bump in friendly");
                    break;
                case "Finish":
                    StartSuccessSequence();
                    break;
                default:
                    StartCrashSequence();
                    break;
            }
        }
    }

    void StartCrashSequence()
    {
        crashVFX.Play();
        audioSource.Stop();
        audioSource.PlayOneShot(crashSFX);
        GetComponent<Movement>().enabled = false;
        Invoke("ReloadLevel", levelLoadDelay);
        isTransitioning = true;
    }
    
    void StartSuccessSequence()
    {
        successVFX.Play();
        audioSource.Stop();
        audioSource.PlayOneShot(successSFX);
        GetComponent<Movement>().enabled = false;
        Invoke("LoadNextLevel", levelLoadDelay);
        isTransitioning = true;
    }

    void ReloadLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }

    public void LoadNextLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;
        if(nextSceneIndex == SceneManager.sceneCountInBuildSettings)
        {
            nextSceneIndex = 0;
        }
        SceneManager.LoadScene(nextSceneIndex);
    }
}
